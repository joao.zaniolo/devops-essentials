FROM node:lts

ENV PORT=3000
EXPOSE 3000

ARG NODE_ENV=production
ENV NODE_ENV=$NODE_ENV

WORKDIR /usr/src/app
COPY package.json /usr/src/app
RUN npm install
COPY . /usr/src/app

CMD [ "npm", "start" ]
